define(function () {

	function controller(modalWidget) {	
		modalWidget.setTitle('Input Client Data');
		modalWidget.setContent(render());	
	}
	
	function render() {
		var hash = location.hash.substring(1);
		var html = ['<form>'];
		html.push('<div class="form-group"><input name="name"  type="name" class="form-control" placeholder="Name"></div>');
		html.push('<div class="form-group"><input name="email" type="email" class="form-control" placeholder="Email"></div>');
		html.push('<div class="form-group"><input name="phone" type="phone" class="form-control" placeholder="Phone (+38099999999)"></div>');
		html.push('<p class="submit"><a class="btn btn-default" href="#book/'+ hash +'" role="button">Ok</a></p>');
		html.push('</form>');
		return html.join('');
	}
	
	return controller;
});