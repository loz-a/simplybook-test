define(['moment', 'app/widget/calendar'], function (moment, calendarWidget) {

	function controller(jsonRpcClient, eventList, eventId, unitId, modalWidget) {	
		var now  = moment();
		var next = moment().add(1, 'months');
		// TODO load timeIntervals in calendar on select next month or year	
		var dateFrom = now.format('YYYY-MM-DD');
		var dateTo   = next.format('YYYY-MM-DD');
		
		var timeIntervals 	  = jsonRpcClient.getAvailableTimeIntervals(dateFrom, dateTo, eventId, unitId);
		var filteredIntervals = getNotEmptyIntervals(timeIntervals, eventList, eventId, unitId);
		
		modalWidget.setTitle('Select Time');
		modalWidget.setContent(render());
			
		calendarWidget(filteredIntervals);
	}
	
	function getNotEmptyIntervals(availableTimeIntervals, eventList, eventId, unitId) {
		var availableTimeFiltered = {}, key, time;	
		for (key in availableTimeIntervals) {
			time = availableTimeIntervals[key][unitId];
			
			if (!time.length) continue;
			// time is array
			var timeAggrigator = [], i, timeFrom, timeTo, diffMs, diffMin, ratio, maxEventDurration;
			
			for (i = 0; i < time.length; i++) {
				timeFrom = moment(key + ' ' + time[i]['from']);
				timeTo   = moment(key + ' ' + time[i]['to']);
				
				diffMs  = timeTo.diff(timeFrom); // diff in milliseconds
				diffMin = moment.duration(diffMs, 'ms').asMinutes(); // diff in minutes
				ratio   = diffMin / eventList[eventId].duration;
				maxEventDurration = diffMin / Math.floor(ratio);
							
				while (!timeFrom.isSameOrAfter(timeTo)) {
					timeAggrigator.push(timeFrom.format('HH:mm'));
					timeFrom.add(moment.duration(maxEventDurration, 'minutes'));
				}
				time[i]['timeAggrigator'] = timeAggrigator;
			}
			availableTimeFiltered[key] = time;			
		}
		return availableTimeFiltered;
	}
	
	
	function render() {
		return '<div class="datetimepicker-box"><input id="datepicker" type="text"></div>';
	};

	return controller;
});