define([
	'moment', 
	'app/form/details', 
	'JsonRpcClient/Exception'
], function (moment, detailsForm, JSONRpcClientException) {

	function controller(JSONRpcClient,eventId, unitId, modalWidget) {	
		var datetime   = moment(time, 'x');
		var bookDate   = datetime.format('YYYY-MM-DD');
		var bookTime   = datetime.format('HH:mm:ss');
		
		
		var $form = modalWidget.find('form');
		var form = detailsForm($form);
		
		if (form.valid()) {
			var result = JSONRpcClient.book(eventId, unitId, bookDate, bookTime, form.getData());
			
			if (result instanceof JSONRpcClientException) {
				modalWidget.find('.submit').after('<p class="text-danger"><strong>Warning!</strong> '+ result.message +'. Correct current data or book <a href="#book-start">again</a></p>');
			} else {
				location.hash = 'book-success';
			}
		} else {
			location.hash = 'invalid-form';
		}
	}
	
	return controller;
});