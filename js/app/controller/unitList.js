define(function () {

	function controller(unitList, eventList, eventId, modalWidget) {	
	
		var units = filterNotMappedUnits(unitList, eventList, eventId);
		
		modalWidget.setTitle('Select Service Provider');
		modalWidget.setContent(render(units));
	}
	
	function filterNotMappedUnits(unitList, eventList, eventId) {
		for (var key in unitList) {		
			if (!(key in eventList[eventId].unit_map)) delete unitList[key];
		}
		return unitList;
	}
	
	function render(data) {
		var hash = window.location.hash.substring(1);
		var html = ['<div class="list-group">'];
		for (var key in data) {
			html.push('<a href="#'+ hash +'/unit/'+ data[key].id +'" class="list-group-item">'+ data[key].name +' (<small>'+ data[key].description +'</small>)</a>');
		}
		html.push('</a>');
		return html.join('');
	};

	return controller;
});