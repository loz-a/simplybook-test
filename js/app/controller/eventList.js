define(function () {

	function controller(eventList, modalWidget) {		
		modalWidget.setTitle('Select Service');
		modalWidget.setContent(render(eventList));	
		modalWidget.show();		
	}
	
	function render(data) {
		var hash, html = ['<div class="list-group">'];
		for (var key in data) {		
			html.push('<a href="#event/'+ data[key].id +'" class="list-group-item">'+ data[key].name +' (<small>duration: '+ data[key].duration +'</small>)</a>');
		}
		html.push('</a>');
		return html.join('');
	};

	return controller;
});