define(function () {

	function controller(modalWidget) {	
		modalWidget.setTitle('Success');
		modalWidget.setContent(render());	
		
		setTimeout(function() {
			modalWidget.hide();
			location.hash = '';
		}, 3000);
	}
	
	function render() {
		return '<p> Congratulations booking was successful </p>';
	}
	
	return controller;
});