define(function (require) {
    	
	var routie 		= require('routie');
	var jrc    		= require('app/service/jsonRpcClient');
	var modalWidget = require('app/widget/modal'); 
	
	var eventList = jrc.getEventList();
	var unitList =  jrc.getUnitList();
	
	routie('book-start', function() {
		var controller = require('app/controller/eventList');
		controller(eventList, modalWidget);
	});
	
	routie('event/:eventId', function(eventId) {
		if (!modalWidget.isVisible()) modalWidget.show();
		
		var controller = require('app/controller/unitList');
		controller(unitList, eventList, eventId, modalWidget);
	});
		
	routie('event/:eventId/unit/:unitId', function(eventId, unitId) {
		if (!modalWidget.isVisible()) modalWidget.show();
		
		var controller = require('app/controller/time');
		controller(jrc, eventList, eventId, unitId, modalWidget);
	});
	
	routie('event/:eventId/unit/:unitId/time/:time', function(eventId, unitId, time) {		
		if (!modalWidget.isVisible()) modalWidget.show();
		
		var controller = require('app/controller/details');
		controller(modalWidget);		
	});
	
	routie('book/event/:eventId/unit/:unitId/time/:time', 
		function(eventId, unitId, time) {
			if (!modalWidget.isVisible()) {
				routie('');
			} else {
				var controller = require('app/controller/book');
				controller(jrc, eventId, unitId, modalWidget);				
			}			
		}
	);
	
	routie('book-success', function() {
		if (!modalWidget.isVisible()) {
			routie('');
		} else {
			var controller = require('app/controller/success');
			controller(modalWidget);			
		}		
	});
});