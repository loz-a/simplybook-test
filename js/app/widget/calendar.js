define(['jquery', 'moment', 'jquery.datetimepicker'], function($, moment) {
	
	function getAllowDates(availableTime) {
		var key, allowDates = [];
		for (key in availableTime) {
			if ((new Date(key)) < Date.now()) continue;
			allowDates.push(key);
		}
		return allowDates;
	}
	
	
	return function(availableTimeIntervals) {
		
		var $datepicker;

		$datepicker = $('#datepicker');
		$datepicker.datetimepicker({
			availableTimeIntervals: availableTimeIntervals,
			inline:true,
			formatDate:'Y-m-d',
			allowDates: getAllowDates(availableTimeIntervals),
			step: 5,
			defaultSelect: false,
			todayButton: false,	
			timepicker: false,			
			onSelectTime: function(currentTime) {
				var timestamp = moment(currentTime).format('x');
				
				location.hash = location.hash.substring(1) + '/time/' + timestamp;
				$datepicker.datetimepicker('destroy');
			},
			onSelectDate: function(currentTime) {
				var timeIntervals    = this.data('options').availableTimeIntervals;
				var currentIntervals = timeIntervals[moment(currentTime).format('YYYY-MM-DD')];
				
				var aggrigators = [];
				for (var i = 0; i<currentIntervals.length; i++) {
					aggrigators.push(currentIntervals[i].timeAggrigator);
				}
				
				this.setOptions({
					timepicker: true,
					allowTimes: Array.prototype.concat.apply([], aggrigators)
				});					
			},
			onChangeMonth: function(currentTime) {
				// TODO
			},
			onChangeYear: function(currentTime) {
				// TODO
			}	
		});
		return $datepicker;
	}
	
});	