define(['jquery', 'bootstrap'], function($) {
	
	function Modal() {		
		var $modal, modalTitle, modalBody, isVisible = false;
	
		$modal = $('#book-modal').modal({show: false});

		$modal.on('hidden.bs.modal', $.proxy(function() {
			isVisible = false;
			this.setTitle('');
			this.setContent('');
			location.hash = '';
		}, this));
		
		$modal.on('shown.bs.modal', function() {
			isVisible = true;
		});
		
		this.isVisible = function() {
			return isVisible;
		};
		
		this.show = function() {
			$modal.modal('show');
		};
		
		this.hide = function() {
			$modal.modal('hide');
		};
		
		this.setTitle = function(text) {
			if (modalTitle === undefined) modalTitle = $modal.find('.modal-title');
			modalTitle.text(text);
			return this;
		};
		
		this.setContent = function(html) {
			if (modalBody === undefined) modalBody = $modal.find('.modal-body');
			modalBody.html(html);
			return this;
		};

		this.find = function(selector) {
			if (modalBody === undefined) modalBody = $modal.find('.modal-body');
			return modalBody.find(selector);
		};		
	}
	
	return new Modal();	
});
