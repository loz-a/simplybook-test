define(['app/config', 'jquery', 'jquery-validate'], function(appConfig, $) {
		
	var config = appConfig.validator.details;
		
	$.validator.setDefaults({
		debug: true,
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-error');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-error');
		},
		errorElement: 'span',
		errorClass: 'help-block',
		errorPlacement: function(error, element) {
			if(element.parent('.input-group').length) {
				error.insertAfter(element.parent());
			} else {
				error.insertAfter(element);
			}
		}
	});
	
	function validate($form) {
		return $form.validate(config);		
	}
	
	return validate;	
});