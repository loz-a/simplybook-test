define({
	rpcClient: {
		url: 'http://user-api.simplybook.me',
		companyLogin: 'learnukrainian',
		apiKey: 'b6f6bc6fdc93435672a2d1cff8166d77b6bf36ca88e4bad145b8fe038fc8c035'
	},
	
	validator: {
		details: {
			rules: {
				name: "required",
				email: {
					required: true,
					email: true
				},
				phone: "required"
			},
			messages: {
				name: "Please specify your name",
				email: {
				  required: "We need your email address to contact you",
				  email: "Your email address must be in the format of name@domain.com"
				}
			}
		}
	}
	
});