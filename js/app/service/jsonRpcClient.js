define(['JsonRpcClient/Client', 'app/config'], function(JSONRpcClient, appConfig) {
	
	var config = appConfig['rpcClient'];
	
	var loginClient = new JSONRpcClient({
		'url': config.url + '/login',
		'onerror': function (error) {},
	});
	var token = loginClient.getToken(config.companyLogin, config.apiKey);

	return new JSONRpcClient({
		'url': config.url,
		'headers': {
			'X-Company-Login': config.companyLogin,
			'X-Token': token
		},
		'onerror': function (error) {}
	});
	
});