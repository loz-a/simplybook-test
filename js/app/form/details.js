define(['app/validator/details'], function(validate) {
		
	function form($form) {
		
		if (!$form.length) {
			throw new Error('No form is selected');
		}
		
		validate($form);
		$form.trigger('submit.validate');
		
		return {
			valid: function() {
				return $form.valid();
			},
			getData: function() {
				var data = {};
				$form.find('input').each(function(idx, node) { data[node.name] = node.value; });
				return data;
			}
		}
	}
	
	return form;	
});