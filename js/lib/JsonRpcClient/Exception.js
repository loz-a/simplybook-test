define(['jquery'], function(jQuery) {
	
	/**
	 * JSON-RPC Client Exception class
	 * 
	 * @param String code
	 * @param String message
	 */
	var JSONRpcClientException = function (code, message) {
		this.code = code;
		this.message = message;
	}
	JSONRpcClientException.prototype = jQuery.extend(JSONRpcClientException.prototype, {

		/**
		 * Magic method. COnvert object to string.
		 * 
		 * @return String
		 */
		toString: function () {
			return '[' + this.code + '] ' + this.message;
		}
		
	});

	return JSONRpcClientException;
});