requirejs.config({
    baseUrl: 'js/lib',
    paths: {
      app: '../app',
	  jquery: 'jquery.min',
	  moment: 'moment.min',
	  'jquery-mousewheel': 'jquery.mousewheel',
	  'jquery-validate': 'jquery.validate'
    },
    shim: {
        'routie': {
			exports: 'routie'
		},
		'bootstrap': ['jquery'],
		'jquery.datetimepicker': ['jquery']
    }
});

// Load the main app module to start the app
requirejs(['app/main']);